package com.company;

import static com.company.GetAPI.*;

public class Main {

    public static void main(String arg[]) throws Exception {
        String exampleMAC = "00:00:2a:01:00:09";

        MapCoordinate location = getLocationByMac(exampleMAC);
        System.out.println("MAIN: X: " + location.getxCoordinate() +
                                " Y: " + location.getyCoordinate());

    }

}