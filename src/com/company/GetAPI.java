package com.company;

import java.io.*;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


/**
 * Created by Air on 21.01.17.
 * @author eaegor
 */
class GetAPI {

    private static String targetURI;
    private static String username;
    private static String password;
    private static JSONObject returnedJsonObjectFromAPI;

    static {
        targetURI = "https://msesandbox.cisco.com:443";
        password = "learning";
        username = "learning";
    }

    static MapCoordinate getLocationBySSID(String ssid) throws Exception {
        String locationAPI = "/api/contextaware/v1/location/clients/";
        String uriAPI = targetURI + locationAPI + ssid;
        sendGETRequest(uriAPI);

        return getMapCoordinateFromJSONObject();
    }

    static MapCoordinate getLocationByMac(String mac) throws Exception {

        String APILocationSSID = "/api/contextaware/v1/location/clients/";
        String uriAPI = targetURI + APILocationSSID + mac;
        sendGETRequest(uriAPI);

        return getMapCoordinateFromJSONObject();
    }

    private static void sendGETRequest(String uriAPI) throws Exception {

        try {
            readUrl(uriAPI);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @NotNull
    private static void readUrl(String urlString) throws Exception {

        JSONParser jsonParser = new JSONParser();

        ProcessBuilder pb = new ProcessBuilder(
                "curl",
                "-i",
                urlString,
                "-XGET",
                "-H",
                "Accept: application/json",
                "--user", username + ":" + password
        );

        pb.redirectErrorStream(true);
        Process p = pb.start();
        InputStream inputStream = p.getInputStream();

        String body = IOUtils.toString(inputStream, "UTF-8");
        String[] lines = body.split("\n");
        returnedJsonObjectFromAPI = (JSONObject)jsonParser.parse(lines[lines.length - 1]);
    }

    private static MapCoordinate getMapCoordinateFromJSONObject () {

        MapCoordinate mapCoorrdinate = new MapCoordinate();

        try {

            // "MapCoordinate":{"x":182.06,"y":35.0,"unit":"FEET"}
            JSONObject mapCoordinateLine = (JSONObject)((JSONObject) returnedJsonObjectFromAPI
                                           .get("WirelessClientLocation"))
                                           .get("MapCoordinate");

            mapCoorrdinate.setxCoordinate((double)mapCoordinateLine.get("x"));
            mapCoorrdinate.setyCoordinate((double)mapCoordinateLine.get("y"));

        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        return mapCoorrdinate;
    }

}
